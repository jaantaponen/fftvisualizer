/*
* Sketch is for controlling led strips from a remote controlled endpoint.
* You can draw bytestreams of NUM_LEDS * 4. The stream will be interepreted as continues repeated sequence of:
* 0 = index
* 1 = red
* 2 = blue
* 3 = green
* 
* Remember to set partition scheme as big app. The bluetooth library is very large.
*
* Jaan Taponen
*/
#include <Arduino.h>
#include <NeoPixelBus.h>
#include <BluetoothSerial.h>

#include <Wire.h>
#include <Adafruit_GFX.h>             //Screen spefic configuration. This can and should be made optional
#include <Adafruit_SSD1306.h>         

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
//Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1); //for wemos
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, 16);

#define NUM_LEDS 57                   // Set to the number of LEDs in your LED strip
#define numChars 1024               // Maximum number of packets to hold in the buffer. Don't change this.
#define PRINT_FPS 0                   // Toggles FPS output (1 = print FPS over serial, 0 = disable output)

//NeoPixelBus settings
const uint8_t PixelPin = 4;           // make sure to set this to the correct pin, ignored for Esp8266(set to 3 by default for DMA)

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif
BluetoothSerial SerialBT;             //The very huge bluetoothSerial object.
//const uint16_t numChars = NUM_LEDS*4;   //The bufferLen for know. This could be increased and thus parsed later, leaving better error handling.
unsigned char receivedChars[numChars];//the byte stream itself, in arduino unsigned char is same as bytearray.
boolean newData = false;

// Wifi and socket settings
/*const char* ssid     = "Mi9";
const char* password = "mustikkaherkku";
unsigned int localPort = 7777;
char packetBuffer[BUFFER_LEN];*/

unsigned char N = 0;
NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> ledstrip(NUM_LEDS, PixelPin);
void setup() {
    Serial.begin(115200);
    //Wire.begin(5, 4); for wemos
    pinMode(16, OUTPUT);  //required by wemos lolin.
    digitalWrite(16, LOW);
    delay(20);
    digitalWrite(16,HIGH);
    Wire.begin(4, 15);

    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C, false, false)) {
      Serial.println(F("SSD1306 allocation failed"));
      for(;;);
    }
    delay(2000);
    display.clearDisplay();
    
    //WiFi.begin(ssid, password);
    SerialBT.begin("ESP32DEV");
    Serial.println("");
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    display.println("Starting bt&wifi...");
    display.display();
    drawInfo();
   
    ledstrip.Begin();//Begin output
    ledstrip.Show();//Clear the strip for use
}

#if PRINT_FPS
    uint16_t fpsCounter = 0;
    uint32_t secondTimer = 0;
#endif

void loop() {
    // Read data over socket
    recvWithStartEndMarkers();
    showNewData();
    #if PRINT_FPS
        if (millis() - secondTimer >= 1000U) {
            secondTimer = millis();
            Serial.printf("FPS: %d\n", fpsCounter);
            drawInfo();
            display.print("FPS: "); display.println(fpsCounter);
            display.display();
            fpsCounter = 0;
        }   
    #endif
}
void showNewData() {
    if (newData == true) {
        for (int i = 0; i < numChars; i+=4) {
          N = receivedChars[i];
          RgbColor pixel((byte)receivedChars[i+1], (byte)receivedChars[i+2], (byte)receivedChars[i+3]);//color
          ledstrip.SetPixelColor(N, pixel);//N is the pixel number
        }
        ledstrip.Show();
        newData = false;
  
        #if PRINT_FPS
            fpsCounter++;
            Serial.print("/");//Monitors connection(shows jumps/jitters in packets)
        #endif
    }
}

void recvWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '{';
    char endMarker = '}';
    char rc;
    while (SerialBT.available() > 1 && newData == false) {
        rc = SerialBT.read();
        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            } else {
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        } else if (rc == startMarker) {
            recvInProgress = true;
        } 
    }
}

void drawInfo() {
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    display.println("(c)Jaan Taponen");
    display.println("LED visualizer");
    //display.println(WiFi.localIP());
    if(SerialBT.hasClient()) {
      display.println("BT: Connected");
    } else {
      display.println("BT: Disconnected");
    }
    display.display();
}
