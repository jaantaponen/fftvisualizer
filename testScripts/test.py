from __future__ import print_function
from __future__ import division

import platform
import numpy as np
import config
import time
import random
import socket
_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)




def _update_esp8266():

    _prev_pixels = np.tile(253, (3, 60)) #_prev_pixels = np.tile(253, (3, config.N_PIXELS))
    """Pixel values that were most recently displayed on the LED strip"""
    #print(_prev_pixels)
    pixels = np.tile(1, (3, 60)) #pixels = np.tile(1, (3, config.N_PIXELS))
    
    #print(pixels)
    """Sends UDP packets to ESP8266 to update LED strip values

    The ESP8266 will receive and decode the packets to determine what values
    to display on the LED strip. The communication protocol supports LED strips
    with a maximum of 256 LEDs.

    The packet encoding scheme is:
        |i|r|g|b|
    where
        i (0 to 255): Index of LED to change (zero-based)
        r (0 to 255): Red value of LED
        g (0 to 255): Green value of LED
        b (0 to 255): Blue value of LED
    """
    #global pixels, _prev_pixels

    # Truncate values and c 
    # ast to integer
    pixels = np.clip(pixels, 0, 255).astype(int)
    # Optionally apply gamma correc tio
    p = np.copy(pixels)
    
    MAX_PIXELS_PER_PACKET = 126
    
    # Pixel indices
    idx = range(pixels.shape[1])
    print(idx)
    idx = [i for i in idx if not np.array_equal(p[:, i], _prev_pixels[:, i])]
    n_packets = len(idx) // MAX_PIXELS_PER_PACKET + 1
    print(n_packets)
    idx = np.array_split(idx, n_packets)
    for packet_indices in idx:
        m = []
        for i in packet_indices:
                
                m.append(i)  # Index of pixel to change
                m.append(210)#m.append(p[0][i])  # Pixel red value
                m.append(70)#m.append(p[1][i])  # Pixel green value
                m.append(20)#m.append(p[2][i])  # Pixel blue value
        print(m)
        m = bytes(m)
        #print(len(m))
        _sock.sendto(m, ('192.168.50.115', 7777)) # _sock.sendto(m, (config.UDP_IP, config.UDP_PORT))
        _prev_pixels = np.copy(p)
    #print(len(m))
        
_update_esp8266()

#while True:
#    time.sleep(.0100)
#    print("sending")
#    _update_esp8266()