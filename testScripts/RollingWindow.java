import be.tarsos.dsp.util.fft.FFT;
import be.tarsos.dsp.util.fft.HannWindow;
import edu.cmu.sphinx.frontend.DoubleData;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.jtransforms.fft.DoubleFFT_1D;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.ops.impl.transforms.Pad;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;

import java.nio.ByteBuffer;

import static org.nd4j.linalg.ops.transforms.Transforms.*;

public class RollingWindow {
    //private static final int samples_per_frame = (int) 44100/60;
    private static final int samples_per_frame = 1024;
    private static final int ROLLING_HISTORY = 2;
    private static final int FFT_BINS = 24;
    private static final int LEDS = 60;
    private INDArray y_roll;
    private int N_original;
    //private FFT fft;
    private DoubleFFT_1D fft;
    private MelFrequencyFilterBank melbank;


    private INDArray prev_spectrum;
    private ExpFilter gain_mel;
    private ExpFilter smoothing_mel;
    private ExpFilter r_filt;
    private ExpFilter g_filt;
    private ExpFilter b_filt;
    private ExpFilter common_mode;
    private ExpFilter gain_value;
    private ExpFilter p;
    private INDArray[] pixels;

    private INDArray mel;
    private INDArray dupMell;

    LinearInterpolator li = new LinearInterpolator(); // or other interpolator

    public RollingWindow() {
        Nd4j.setDataType(DataType.DOUBLE);
        //y_roll = Nd4j.rand(ROLLING_HISTORY, samples_per_frame);
        y_roll = Nd4j.rand(ROLLING_HISTORY, samples_per_frame).divi(1e16); //for testing
        prev_spectrum = Nd4j.zeros(LEDS/2).addi(0.01);//np.tile(0.01, config.N_PIXELS // 2)

        gain_mel = new ExpFilter(Nd4j.zeros(FFT_BINS).addi(1e-1), 0.01, 0.99);
        smoothing_mel = new ExpFilter(Nd4j.zeros(FFT_BINS).addi(1e-1), 0.5, 0.99);
        common_mode = new ExpFilter(Nd4j.zeros(LEDS/2).addi(0.01), 0.99,0.01);
        gain_value = new ExpFilter(Nd4j.zeros(FFT_BINS).addi(1e-1), 0.01,0.99);
        r_filt = new ExpFilter(Nd4j.zeros(LEDS/2).addi(0.01), 0.2,0.99);
        g_filt = new ExpFilter(Nd4j.zeros(LEDS/2).addi(0.01), 0.05,0.3);
        b_filt = new ExpFilter(Nd4j.zeros(LEDS/2).addi(0.01), 0.1,0.5);
    }

    public void run(INDArray y)  {
        //y = y.div(Math.pow(2.0, 15));
        INDArray y_data = updateRollingWindow(y);
        INDArray mel = calculateFFTAndMel(y_data);
        pixels = visualizeSpectrum(mel);
        parseForStrip(pixels);
    }

    public void run(double[] audioSamples)  {
        INDArray y = Nd4j.create(audioSamples);
        INDArray y_data = updateRollingWindow(y);

    }
    private INDArray calculateFFTAndMel(INDArray y_data) {
        double[] fftResult = y_data.data().asDouble();
        if(fft == null) {
            //fft = new FFT(fftResult.length, new HannWindow());
            fft = new DoubleFFT_1D(fftResult.length);
        }
        fft.realForward(fftResult);
        //fft.forwardTransform(fftResult);


        /*double[] tmp = new double[fftResult.length];
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = fftResult[i];
        }*/

        INDArray fftTemp = Nd4j.create(fftResult);
        fftTemp = abs(fftTemp.get(NDArrayIndex.interval(1, (N_original / 2) + 1))); //strip out DC and cut at Nyquist(not from padded values)

        if(melbank == null)
            melbank = new MelFrequencyFilterBank(200, 12000, FFT_BINS);
        DoubleData melbankDst = melbank.process(new DoubleData(fftTemp.data().asDouble(), 44100, 0));
        INDArray melbankFilter = Nd4j.create(melbankDst.getValues());

        /*mel = np.atleast_2d(YS).T * dsp.mel_y.T
        *    >>> a = numpy.array([1,2,3,4])
        *    >>> b = numpy.array([5,13])
        *    >>> numpy.atleast_2d(a).T * b.T
        *    array([[ 5, 13],
        *           [10, 26],
        *           [15, 39],
        *           [20, 52]])
        *    >>> a = numpy.array([1,2,3,4,5])
        *    >>> numpy.atleast_2d(a).T * b.T
        *    array([[ 5, 13],
        *           [10, 26],
        *           [15, 39],
        *           [20, 52],
        *           [25, 65]])
        *   Creating a matrix where row len is defined by the FFT data and col len by melbank(24 in this case).
        *   Then placing melbank values to each row and multiplying with next fftvalue.
        *   In case of where fftData is 765 long(stripped from dc/nyquist and the added padding lenght) and
        *   the FFT bins are set to 24 the end result is 765 x 24 matrix. (Numpy len() only reports the row count).
        * */

        //mel = Nd4j.zeros((int)fftTemp.length(),(int)melbankFilter.length());

        mel = fftTemp.reshape(fftTemp.length(), 1).mmul(melbankFilter.reshape(1, melbankFilter.length()));

        /*for (int i = 0; i < mel.rows(); i++) {
            mel.getRow(i)
                    .assign(melbankFilter)
                    .mul(fftTemp.getDouble(i)); //we can use this for 1d arrays (current alpha(06) has a bug where fetching 2d values is unreliable
        }*/

        mel = pow(mel.sum(0),2);
        //mel = mel.sum(0); //uncomment to reduce scale(easier to read)
        //normalize(mel);
        System.out.println("\n\nMel wihtout gain correction");
        System.out.println(mel.length());
        System.out.println(mel);

        gain_mel.update(Nd4j.zeros(mel.length()).addi(mel.maxNumber())); //setting gain updates.
        mel = mel.div(gain_mel.getValue());
        System.out.println("\n\nMel with gain correction");
        System.out.println(mel);

        smoothing_mel.update(mel);
        System.out.println(smoothing_mel.getValue());
        mel = smoothing_mel.getValue(); //this doesn't have much effect if gain has not been gaussianfiltered.
        System.out.println("\n\nMel with SMOOTHING");
        System.out.println(mel);
        //melbankFinished = melbankFinished.reshape(melbankFinished.length(), 1);
        //System.out.println(melbankFinished);
        return mel;
    }

    private void normalize (INDArray a) {
        double max = (double) a.minNumber();
        double min = (double) a.maxNumber(); //Why are these the other way around? could be a bug in the library.
        double[] aNormalized = new double[(int)a.length()];
        for(int i = 0; i < aNormalized.length; i++) {
            double normalized = 1.0 - ((a.getDouble(i) - min) / (max - min));
            aNormalized[i] = normalized;
        }
        a.assign(Nd4j.create(aNormalized));
    }

    private INDArray updateRollingWindow(INDArray y) {
        int lastRow = y_roll.rows();
        y_roll.get(NDArrayIndex
                .interval(0,lastRow-1), NDArrayIndex.all())
                .assign(y_roll.get(NDArrayIndex
                        .interval(1,lastRow), NDArrayIndex.all()));
        System.out.println("Y_roll1:");
        System.out.println(y_roll);
        System.out.println("");

        y_roll.get(NDArrayIndex.interval(lastRow-1, lastRow)).assign(y.dup());
        System.out.println("Y_roll2: \n");
        System.out.println(y_roll);
        //System.out.println(y_roll.getDouble(0,2));
        System.out.println("");

        //INDArray y_data = Nd4j.toFlattened(y_roll);
        INDArray y_data = y_roll.getRow(0);
        for (int i = 1; i < y_roll.rows(); i++) {
            y_data = Nd4j.hstack(y_data, y_roll.getRow(i));
        }

        System.out.println("y_data: ");
        System.out.println(y_data);
        //printINDA(y_data);

        int N = (int)y_data.length();
        N_original = N;
        if(!isPowerOfTwo(N)) {
            N = closestPowerOfTwoAbove(N);
            int sides = (int)(N-y_data.length());
            y_data= Nd4j.pad(y_data, new int[]{0, sides}, Pad.Mode.CONSTANT, 0);
        }
        System.out.println("\n y_padded");
        System.out.println(y_data);
        System.out.println("Length of padded was: "+ y_data.length());
        return y_data;
    }

    private INDArray[] visualizeSpectrum(INDArray y) {
        y = interpolate(y, LEDS / 2);
        common_mode.update(y);
        INDArray diff = y.dup().sub(prev_spectrum);
        prev_spectrum = y.dup();

        r_filt.update(y.sub(common_mode.getValue()));
        INDArray r = r_filt.getValue();

        INDArray g = abs(diff);

        b_filt.update(y);
        INDArray b = b_filt.getValue();
        r = r.mul(255); g = g.mul(255); b = b.mul(255);
        return new INDArray[]{r, g, b};
    }

    /*
    * "Intelligently" scaling arrays.
    * https://stackoverflow.com/questions/36523396/interpolate-function-using-apache-commons-math/36523685
    *
    * */
    private INDArray interpolate(INDArray y, int newLength) {
        if (y.length() == newLength)
            return y;
        INDArray x_old = normalized_linspace((int)y.length()); //this could be an issue if the len returns wrong
        INDArray x_new = normalized_linspace(newLength);
        PolynomialSplineFunction psf = li.interpolate(x_old.data().asDouble(), y.data().asDouble());

        double[] xi = x_new.data().asDouble();
        double[] yi = new double[xi.length];
        for (int i = 0; i < xi.length; i++) {
            yi[i] = psf.value(xi[i]);
            //System.out.println(yi[i]);
        }
        //System.out.print(yi.length);
        return Nd4j.create(yi);
    }
    private byte[] parseForStrip(INDArray[] pixels) {
        if (pixels.length != 3)
            throw new IllegalArgumentException();
        int[] r = pixels[0].data().asInt();
        int[] g = pixels[1].data().asInt();
        int[] b = pixels[2].data().asInt();
        if (r.length != g.length || g.length != b.length)
            throw new IllegalArgumentException();

        int len = (int) pixels[0].length();
        String prnt = "";

        byte[] allByteArray = new byte[len*4];
        ByteBuffer buff = ByteBuffer.wrap(allByteArray);
        for (int i = 0; i < len; i++) {
            byte test1 = (byte) (i & 0xff);
            prnt += test1 + " Red " + (r[i]) + ", Green " + g[i] + ", Blue" + b[i] + "\n";
            byte test2 = (byte) (r[i] & 0xff);
            byte test3 = (byte) (g[i] & 0xff);
            byte test4 = (byte) (b[i] & 0xff);
            byte[] listTest = {test1,test2,test3,test4};
            buff.put(listTest);
        }
        System.out.println(prnt);
        return buff.array();
    }
    private INDArray normalized_linspace(int size) {
        return Nd4j.linspace(0, 1, size);
    }
    public int getSamples_per_frame() {
        return samples_per_frame;
    }

    public INDArray[] getPixels() {
        return pixels;
    }
    private static int closestPowerOfTwoAbove(int N) {
        return 1 << (int) Math.ceil(Math.log(N) / Math.log(2)); }
    private static boolean isPowerOfTwo(int N) {
        final int maxBits = 32;
        int n = 2;
        for (int i = 2; i <= maxBits; i++) {
            if (n == N)
                return true;
            n <<= 1;
        }
        return false;
    }
}
