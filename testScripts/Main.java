import be.tarsos.dsp.util.fft.FFT;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.conditions.Conditions;


public class Main {
    public static void main(String[] args) {
        Nd4j.setDataType(DataType.DOUBLE);
        RollingWindow r = new RollingWindow();

        /*double[] tmp = new double[]{1,2,3,};
        INDArray a = Nd4j.create(tmp);
        tmp = new double[]{5,13,14};
        INDArray b = Nd4j.create(tmp);

        tmp = new double[]{7,8,9};
        INDArray c = Nd4j.create(tmp);

        c.replaceWhere(Nd4j.ones(c.length()), Conditions.greaterThan(7));
        System.out.println(c);

        System.out.println(a);
        System.out.println(b);
        a = a.reshape(a.length(), 1);
        b = b.reshape(1, b.length());



        //INDArray c = Nd4j.create(4,3);
        INDArray c = a.mmul(b);
        System.out.println("");
        System.out.println("");
        System.out.println(c);*/
        //INDArray rand = Nd4j.rand(r.getSamples_per_frame()).muli(127);*/
        INDArray rand = Nd4j.zeros(r.getSamples_per_frame()).addi(40);



       while(true) {
            try{

                r.run(rand);
                Thread.sleep(1000);
            }catch(InterruptedException ex){
                //do stuff
            }
        }

    }
}
