[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/janetsqy/fftvisualizer">
    <img src="https://gitlab.com/janetsqy/fftvisualizer/-/wikis/uploads/5e2e78e676ef836831b2adaa9a6ebd80/logo.png" alt="Logo">
  </a>

  <h3 align="center">Led Visualizer</h3>

  <p align="center">
    Visualize your led strips wirelessly with wifi or bluetooth! Either straight from your Android phone or device that runs Java.
    <br />
    <a href="https://gitlab.com/janetsqy/fftvisualizer/-/wikis/Project-motivation"><strong>Explore the wikis! »</strong></a>
    <br />
    <br />
    <a href="https://github.com/github_username/repo">View Demo</a>
    ·
    <a href="https://gitlab.com/janetsqy/fftvisualizer/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/janetsqy/fftvisualizer/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)
* [Contributing](#contributing)



<!-- ABOUT THE PROJECT -->
## About The Project

I started this project to learn about Android app development and figured this shouldn't be too much of a hassle to botch together. I very quickly realized music visualization is no joke and involves a fair bit of Math. I quickly learned about the following:
* Android runtime ecosystem
* Fast, discrete and partial Fourier Transforms.
* Mel Filtering
* Interpolation
* Matrix operations (these were quite a bit more challenging than in Python)

Even though there are some all in solutions available for Java many of them don't produce a clear Sine wave and have a lot of spectral leakage so in the end I had to combine many different things. You can read more of my learning journey in the [wikis](https://gitlab.com/janetsqy/fftvisualizer/-/wikis/home)

## How the algorithm works
<p align="center">
  <a href="https://gitlab.com/janetsqy/fftvisualizer/-/wikis/Technical-Specifications">
    <img src="https://gitlab.com/janetsqy/fftvisualizer/-/wikis/uploads/d918da068d7f51838f112e827a158b2e/skeema_kok.png" width="800px" alt="Logo">

</p>
The infograph is read from left to right.

 _If you want to learn more about the signal processing head over to the, head over to to the [Documentation](https://gitlab.com/janetsqy/fftvisualizer/-/wikis/Usage#esp32-flashing)_

### Built With

* Java
* C++
* [NDJ4](https://deeplearning4j.org/)


<!-- GETTING STARTED -->
## Getting Started

You can either run the algorithm/simple UI on your Java capable device or download the APK from here. TODO You will also need a combatible ESP-xx device. I would recommend WS8211B type led strips but others will work too.

### Prerequisites

**Gradle** will take care of all the dependencies the project uses. Some apps that make using it very simple. I recommend using IntelliJ IDEA.
* Android phone for the app
* Bluetooth/WiFi cabable device (with support and power to run Java).
* [ESP32 with or without screen](https://www.banggood.com/LILYGO-TTGO-T-Display-ESP32-CP2104-WiFi-bluetooth-Module-1_14-Inch-LCD-Development-Board-p-1522925.html?rmmds=myorder&cur_warehouse=CN)

### Installation
 
1. Clone the repo
```sh
git clone https://gitlab.com/janetsqy/fftvisualizer
```
Or Install the APK TODO

2. Flash the Arduino sketch to your ESP32. Remember to edit the LED variable to reprsesent your configuration.
3. Connect you LEDs to the device correctly.

_For more in depth wiring and flashing instructions, refer to the [Documentation](https://gitlab.com/janetsqy/fftvisualizer/-/wikis/Usage#esp32-flashing)_

<!-- USAGE EXAMPLES -->
## Usage

Escpecially using the Android app is very straight forward.
1. Pair your Android device with your ESP32.
2. Choose your device from the list. THen open the **visualizer** tab.
3. Start playing music and watch your beats come to life.

The standalone Java code uses WiFi to control the strip
1. Compile the app (I recommend IntelliJ).
2. Run the application and enter your ESP32 ip-address.
3. Start playing music.

_For more examples, please refer to the [Documentation](https://gitlab.com/janetsqy/fftvisualizer/-/wikis/Usage#application-usage)_


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- CONTACT -->
## Contact

Jaan Taponen - jaan.taponen@hotmail.com 
Project Link: [https://gitlab.com/janetsqy/fftvisualizer](https://gitlab.com/janetsqy/fftvisualizer)


<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

* [Scott Lawson's great implementation using python](https://github.com/scottlawsonbc/audio-reactive-led-strip)
* [This very clear representation of Bluetooth servicing in Andoid](https://github.com/kai-morich/SimpleBluetoothTerminal)
* [JTransforms](https://github.com/wendykierp/JTransforms)
* [TarsosDSP](https://github.com/JorenSix/TarsosDSP)


<!-- CONTRIBUTING -->
## Contributing

I made this app mainly for my friends and to be put on a Finnish student overalls, but contributions and developmental ideas are **highly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=flat-square
[license-url]: https://gitlab.com/janetsqy/fftvisualizer/blob/master/LICENSE
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/jaan-taponen
[product-screenshot]: images/screenshot.png
