package com.example.fftvisualizer.fragment;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.example.fftvisualizer.R;

import java.util.List;

//TODO REMOVE THIS
public class VisualizerRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Object> contents;


    static final int TYPE_VISUALIZER = 0;


    public VisualizerRecyclerViewAdapter(List<Object> contents) {
        this.contents = contents;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_VISUALIZER;
            default:
                return TYPE_VISUALIZER;
        }
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        switch (viewType) {
            case TYPE_VISUALIZER: {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.credit_fragment, parent, false);
                return new RecyclerView.ViewHolder(view) {
                };
            }

        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_VISUALIZER:
                break;
        }
    }
}