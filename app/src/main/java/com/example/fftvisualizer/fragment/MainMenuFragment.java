package com.example.fftvisualizer.fragment;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.fftvisualizer.R;
import com.example.fftvisualizer.tools.bluetooth.SerialListener;
import com.example.fftvisualizer.tools.bluetooth.SerialService;
import com.example.fftvisualizer.visualizer.VisualizerService;

import java.util.ArrayList;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import kotlin.Unit;

public class MainMenuFragment extends Fragment implements ServiceConnection, SerialListener {
    private enum Connected { False, Pending, True }
    private VisualizerService service;
    private boolean initialStart = true;
    private Connected connected = Connected.False;

    private static final int REQUEST_ENABLE_BT = 1;
    public String deviceAddress;
    public String connectionStatus;
    private BluetoothAdapter bluetoothAdapter;
    private ArrayList<BluetoothDevice> listItems = new ArrayList<>();
    private ArrayAdapter<BluetoothDevice> listAdapter;
    private Spinner btSpinner;
    private CircularProgressButton loadVisualizer;
    private CircularProgressButton btn;
    private TextView statusText;

    public static MainMenuFragment newInstance() {
        return new MainMenuFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH))
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        btSpinner = view.findViewById(R.id.deviceSpinner);
        listAdapter = new ArrayAdapter<BluetoothDevice>(getActivity(), android.R.layout.simple_spinner_item, listItems);

        listAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        btSpinner.setAdapter(listAdapter);
        btSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                //Hack to actually show the selection
                TextView selectedText = (TextView) adapterView.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(Color.BLACK);
                }
                BluetoothDevice item = (BluetoothDevice) adapterView.getItemAtPosition(position);
                deviceAddress = item.getAddress();
                btSpinner.setSelection(position);
                Log.v("item", item.getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // TODO Auto-generated method stub

            }
        });
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.baseline_settings_bluetooth_white_48dp);
        btn = view.findViewById(R.id.imgBtnTest0);
        btn.setText("CONNECT");

        btn.setOnClickListener(v -> {
            if (connected == Connected.False) {
                btn.startAnimation();
                status("TRYING TO CONNECT TO " + deviceAddress);
                connect();
            } else if (connected ==Connected.True) {
                btn.startAnimation();
                status("TRYING TO DISCONNECT FROM " + deviceAddress);
                disconnect();
            }
        });
        loadVisualizer = view.findViewById(R.id.visualizerStatusBtn);
        loadVisualizer.startAnimation();
        statusText = view.findViewById(R.id.statusTextView);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d("VISUAL", "Onattach");
        getActivity().bindService(new Intent(getActivity(), VisualizerService.class), this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDestroy() {
        if (connected != Connected.False)
            disconnect();
        getActivity().stopService(new Intent(getActivity(), VisualizerService.class));
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(service != null) {
            service.attach(this);
        } else {
            Log.d("VISUAL", "Onstart");
            // prevents service destroy on unbind from recreated activity caused by orientation change
            getActivity().startService(new Intent(getActivity(), VisualizerService.class));
        }
    }

    @Override
    public void onStop() {
        if(service != null)
            service.detach();
        super.onStop();
    }


    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        service = (VisualizerService) ((SerialService.SerialBinder) binder).getService();
        //
        if(initialStart && isResumed()) {
            initialStart = false;
            status("Visualizer service connected.");
            service.setPlayerThreaded(0);
            /*
            * For now. TODO
            * */

            new Thread(() -> {
                try { while (!service.initialized()) { Thread.sleep(200); }
                } catch (InterruptedException e) { e.printStackTrace(); }
                getActivity().runOnUiThread(() -> {
                    Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_done_white_48dp);
                    loadVisualizer.doneLoadingAnimation(Color.GREEN, bm);
                });

            }).start();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        service = null;
    }


    @Override
    public void onDetach() {
        try { getActivity().unbindService(this); } catch(Exception ignored) {}
        super.onDetach();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (bluetoothAdapter == null) {
            //setEmptyText("<bluetooth not supported>");
        } else if (!bluetoothAdapter.isEnabled()) {
            //setEmptyText("<bluetooth is disabled>");
        } else {
            //setEmptyText("<no bluetooth devices found>");
        }
        refresh();
    }
    void refresh() {
        listItems.clear();
        if (bluetoothAdapter != null) {
            for (BluetoothDevice device : bluetoothAdapter.getBondedDevices()) {
                if (device.getType() != BluetoothDevice.DEVICE_TYPE_LE) {
                    listItems.add(device);
                }
            }
        }
        listAdapter.notifyDataSetChanged();
    }

    /*
     * Serial + UI
     */
    private void connect() {
        try {
            BluetoothDevice device = bluetoothAdapter.getRemoteDevice(deviceAddress);
            String deviceName = device.getName() != null ? device.getName() : device.getAddress();
            status("connecting...");
            connected = Connected.Pending;
            service.connect(this, "Connected to " + deviceName);
            service.connectSocket(this, deviceName, getContext(), device);

            //service.startVisualizing();
        } catch (Exception e) {
            onSerialConnectError(e);
        }
    }

    private void disconnect() {
        service.stopVisualize();
        connected = Connected.False;
        btn.revertAnimation(() -> {
            btn.setText("CONNECT");
            return Unit.INSTANCE; //for kotlin compability
        });
        service.disconnect();
        service.disconnectSocket(); //TODO maybe redundant
        status("Disconnected from " + deviceAddress);
    }

    private void status(String s) {
        Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
        statusText.setText(s);
    }

    /*
     * SerialListener
     */
    @Override
    public void onSerialConnect() {
        status("connected");
        connected = Connected.True;
        //btn.revertAnimation();
        btn.revertAnimation(() -> {
            btn.setText("DISCONNECT");
            service.startVisualizing();
            return Unit.INSTANCE; //for kotlin compability
        });
    }

    @Override
    public void onSerialConnectError(Exception e) {
        status("connection failed: " + e.getMessage());
        disconnect();
    }

    @Override
    public void onSerialRead(byte[] data) {
        //receive(data);
    }

    @Override
    public void onSerialIoError(Exception e) {
        status("connection lost: " + e.getMessage());
        disconnect();
    }

    /**
     * sort by name, then address. sort named devices first
     */
    static int compareTo(BluetoothDevice a, BluetoothDevice b) {
        boolean aValid = a.getName() != null && !a.getName().isEmpty();
        boolean bValid = b.getName() != null && !b.getName().isEmpty();
        if (aValid && bValid) {
            int ret = a.getName().compareTo(b.getName());
            if (ret != 0) return ret;
            return a.getAddress().compareTo(b.getAddress());
        }
        if (aValid) return -1;
        if (bValid) return +1;
        return a.getAddress().compareTo(b.getAddress());
    }
}
