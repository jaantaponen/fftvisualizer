package com.example.fftvisualizer.old;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.fftvisualizer.visualizer.RollingWindow;

import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import be.tarsos.dsp.util.fft.HannWindow;
import be.tarsos.dsp.util.fft.WindowFunction;

import static android.media.audiofx.Visualizer.MEASUREMENT_MODE_PEAK_RMS;
import static android.media.audiofx.Visualizer.SCALING_MODE_AS_PLAYED;

abstract public class BaseVisualizer extends View {
    private static final int fps = 60;
    private static final long WAIT_UNTIL_HACK = 1000; // fix from https://github.com/Cleveroad/WaveInApp/blob/f6fd11a8f8a20e1ee01999db3d80b5037a773f08/library/src/main/java/com/cleveroad/audiovisualization/VisualizerWrapper.java#L37
    private long lastZeroArrayTimestamp;
    protected byte[] bytes;
    protected Paint paint;
    protected Visualizer visualizer;
    protected int color = Color.BLUE;
    protected float[] magnitudes;
    protected float[] decibels;
    protected float[] phases;
    protected float[] fftData;
    protected WindowFunction usedWindow;
    protected boolean initialized= false;
    private double[] fftTemp;
    protected INDArray[] pixels;

    protected RollingWindow rollingWindow;


    public BaseVisualizer(Context context) {
        super(context);
        Nd4j.setDataType(DataType.DOUBLE);
        init(null);
        init();
    }

    public BaseVisualizer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Nd4j.setDataType(DataType.DOUBLE);
        init(attrs);
        init();
    }

    public BaseVisualizer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Nd4j.setDataType(DataType.DOUBLE);
        init(attrs);
        init();
    }

    private void init(AttributeSet attributeSet) {
        paint = new Paint();
        usedWindow = new HannWindow();
    }

    public void setColor(int color) {
        this.color = color;
        this.paint.setColor(this.color);
    }

    @Deprecated
    public void setPlayer(MediaPlayer mediaPlayer) {
        setPlayer(mediaPlayer.getAudioSessionId());
    }

    public void setPlayer(int audioSessionId) {
        visualizer = new Visualizer(0);
        visualizer.setEnabled(false);

        //visualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        visualizer.setCaptureSize(44100/fps);
        //visualizer.setCaptureSize(765);

        this.visualizer.setScalingMode(SCALING_MODE_AS_PLAYED);
        this.visualizer.setMeasurementMode(MEASUREMENT_MODE_PEAK_RMS);
        //this.visualizer.setMeasurementMode(MEASUREMENT_MODE_PEAK_RMS);
       /*visualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            @Override
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes,
                                              int samplingRate) {
                boolean allZero = Utils.allElementsAreZero(bytes);
                if (lastZeroArrayTimestamp  == 0) {
                    if (allZero) {
                        lastZeroArrayTimestamp = System.currentTimeMillis();
                    }
                } else  {
                    if (!allZero) {
                        lastZeroArrayTimestamp = 0;
                    } else if (System.currentTimeMillis() - lastZeroArrayTimestamp >= WAIT_UNTIL_HACK) {
                        setEnabled(true);
                        lastZeroArrayTimestamp = 0;
                    }
                }
                if(initialized) {
                    //calculate(bytes);
                }
                //invalidate();
            }
            @Override
            public void onFftDataCapture(Visualizer visualizer, byte[] bytes,
                                         int samplingRate) {

            }
        }, getMaxCaptureRate(), true, false);*/


        visualizer.setEnabled(true);
        //byte[] tmp = new byte[visualizer.getCaptureSize()];
        byte[] tmp = new byte[visualizer.getCaptureSize()];
        visualizer.getWaveForm(tmp);
        initialize(tmp);

        Thread thread = new Thread() {
            @Override
            public void run() {
                while(true) {
                    byte[] tmp = new byte[visualizer.getCaptureSize()];
                    visualizer.getWaveForm(tmp);
                    calculate(tmp);
                    invalidate();
                }
            }
        };thread.start();

    }


    public void setWindow(WindowFunction window) {
        usedWindow = window;
    }


    private void calculate(byte[] bytes) {
        if (!initialized) {
            return;
        }
        int n = bytes.length;
        if (fftData == null)
            fftData = new float[n];
        if (fftTemp == null)
            fftTemp = new double[n];

        for (int i = 0; i < n; i++) {
            int tmp = (int) (bytes[i]) & 0xFF;
            fftData[i] = (float) ((tmp) - 128);
            fftTemp[i] = fftData[i];
        }
        rollingWindow.run(fftData);
        //pixels = rollingWindow.getPixels();
        //thread.start();
        //pixels = visualizeSpectrum(melbank);
        //generateMagAndDB();
        BaseVisualizer.this.bytes = bytes;
    }

    private void generateMagAndDB() {
        //magnitudes from raw fft data to decibel form
        //https://developer.android.com/reference/android/media/audiofx/Visualizer#getFft(byte%5B%5D)
        int n = fftData.length;
        if (magnitudes == null)
            magnitudes = new float[n / 2 + 1];
        if (phases == null)
            phases = new float[n / 2 + 1];
        if (decibels == null)
            decibels = new float[n / 2 + 1];

        magnitudes[0] = (float) Math.abs(fftData[0]);      // DC
        magnitudes[n / 2] = (float) Math.abs(fftData[1]);  // Nyquist
        phases[0] = phases[n / 2] = 0;
        for (int k = 1; k < n / 2 - 1; k++) {
            double re = fftData[2 * k];
            double im = fftData[2 * k + 1];
            double sqMag = re * re + im * im;
            decibels[k] = (float) (20 * Math.log10(sqMag));
            int i = k * 2;
            magnitudes[k] = (float) Math.hypot(fftData[i], fftData[i + 1]);
            phases[k] = (float) Math.atan2(fftData[i + 1], fftData[i]);
        }
       /*if (magnitudes == null)
            magnitudes = new float[n / 2];
        if (phases == null)
            phases = new float[n / 2];
        if (decibels == null)
            decibels = new float[n / 2];*/
        //SineGenerator sinA = new SineGenerator(1200, 44100, 10);
        //sinA.getSamples(a);

        //fft.powerAndPhaseFromFFT(fftData, magnitudes, phases);
    }

    //the melbank needs a quite a bit of time to initialize.
    public void initialize(byte[] bytes) {
        Log.d("STATE", "BYTES NULL " + bytes.length);
        Log.d("STATE","INITIALIZATION STARTED NOW!");
        float[] tmpA = new float[bytes.length];
        double[] tmpB = new double[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            int tmp = (int) (bytes[i]) & 0xFF;
            tmpA[i] = (float) ((tmp) - 128);
            tmpB[i] = tmpA[i];

        }

        rollingWindow = new RollingWindow(tmpB.length);
        //pixels = rollingWindow.getPixels();
        initialized = true;
    }

    public void release() {
        visualizer.release();
        bytes = null;
        invalidate();
    }

    public Visualizer getVisualizer() {
        return visualizer;
    }


    protected abstract void init();
}