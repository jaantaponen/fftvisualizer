package com.example.fftvisualizer.old;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.Nullable;


//https://github.com/eclipse/deeplearning4j-examples/blob/master/nd4j-examples/src/main/java/org/nd4j/examples/numpy_cheatsheat/NumpyCheatSheat.java
public class BarVisualizer extends BaseVisualizer {
    private int density = 64;
    private int BASE_SCALE = 1024;
    private int gap;
    private boolean logScale;
    private boolean setLinearVisualization = false;

    public interface OnEventListener {
        // you can define any parameter as per your requirement
        public void onDataReady(byte[] data);
    }
    private OnEventListener mOnEventListener;

    public void setOnEventListener(OnEventListener listener) {
        mOnEventListener = listener;
    }

    public void doEvent() {

        if (initialized){
            //mOnEventListener.onDataReady(rollingWindow.parseForStrip(pixels));
        }

    }

    public BarVisualizer(Context context) {
        super(context);
    }

    public BarVisualizer(Context context,
                         @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BarVisualizer(Context context,
                         @Nullable AttributeSet attrs,
                         int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        this.gap = 2; //gap between
        paint.setStyle(Paint.Style.FILL);
        logScale = false;
    }

    public void setDensity(int density) {
        this.density = density;
        if (density > 512) {
            this.density = 512;
        } else if (density < 10) {
            this.density = 10;
        }
    }

    public int getDensity() {
        return density;
    }

    public void setGap(int gap) {
        if (gap > 0 && gap < 20) {
            this.gap = gap;
        }
    }

    public void setLinearVisualization(boolean a) {
        setLinearVisualization = a;
    }

    public void setBASE_SCALE(int limit) {
        if (limit > 0)
            BASE_SCALE = limit;
    }
    public int getBASE_SCALE() {
        return BASE_SCALE;
    }

    public void setLogScale(boolean value) {
        logScale = value;
    }

    public boolean getLogValue() {
        return logScale;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Canvas draw = setLinearVisualization ? lineaDraw(canvas) : graphDraw(canvas);
        super.onDraw(draw);

    }

    private Canvas graphDraw(Canvas canvas) {
        float[] scale = logScale ? decibels : magnitudes;
        //float[] scale = fftData;

        if (scale != null) {
            String print = "";
            float barWidth = getWidth() / density;
            float div = scale.length / density;
            paint.setStrokeWidth(barWidth - gap);

            for (int i = 2 ; i < scale.length; i++) { //hard limit before repeating starts
                float amp = scale[i];

                //print += amp + ", ";
                //float y = processing.core.PApplet.map(amp, 0, BASE_SCALE, getHeight(),0);
                float barX = (i * barWidth) + (barWidth / 2);
                //canvas.drawLine(barX, getHeight(), barX, y, paint);
            }
            Log.d("STATE", print);

            if (pixels != null) {
                doEvent();
            }
        }
        return canvas;
    }

    private Canvas lineaDraw(Canvas canvas) {
        //Log.d("STATE", "PIXELS NULL : " + (pixels == null));

        if (pixels !=null) {
            int[] r = pixels[0].data().asInt();
            int[] g = pixels[1].data().asInt();
            int[] b = pixels[2].data().asInt();
            if (r.length != g.length || g.length != b.length)
                throw new IllegalArgumentException();
            int pixelLen = r.length;

            float barWidth = getWidth() / pixelLen;
            paint.setStrokeWidth(barWidth - gap);
            for(int i = 0; i < r.length; i++) {
                int color = Color.rgb(r[i], g[i], b[i]);
                //Log.d("COLOR", "Red " + rb + ",  Green " + gb + ",  Blue " + bb);
                paint.setColor(color);
                float barX = (i * barWidth) + (barWidth / 2);
                canvas.drawLine(barX, 0, barX, getHeight(), paint);
            }
            doEvent();
        }
        return canvas;
    }

}
