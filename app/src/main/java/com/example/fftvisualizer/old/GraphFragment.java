package com.example.fftvisualizer.old;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.fftvisualizer.R;

import be.tarsos.dsp.util.fft.BartlettHannWindow;
import be.tarsos.dsp.util.fft.BlackmanHarrisNuttall;
import be.tarsos.dsp.util.fft.HammingWindow;
import be.tarsos.dsp.util.fft.HannWindow;
import be.tarsos.dsp.util.fft.ScaledHammingWindow;


public class GraphFragment extends Fragment {
    private MediaPlayer mediaPlayer;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //mediaPlayer.setLooping(false);
        return inflater.inflate(R.layout.old_graph_fragment, container, false);
    }

    OnDataSendListener callback;
    public void setOnDataSendListener(OnDataSendListener callback) {
        this.callback = callback;
    }

    public interface OnDataSendListener {
        void onDataSend(byte[] data);

        void onConnect();

        void onDisconnect();

        String onStatus();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //BarVisualizer barVisualizer = view.findViewById(R.id.visualizerz);
        BarVisualizer barVisualizer = view.findViewById(R.id.lineV);
        barVisualizer.setLinearVisualization(true);

        // set custom color to the line.
        barVisualizer.setColor(ContextCompat.getColor(getActivity(), R.color.blue));

        // Set your media player to the visualizer.
        //barVisualizer.setPlayer(mediaPlayer.getAudioSessionId());
        barVisualizer.setPlayer(0);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        barVisualizer.setOnEventListener(data -> {
            callback.onDataSend(data);       //just relay data back to Controlleractivity
        });


        final Switch logSwitch = view.findViewById(R.id.log_scale);
        logSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    barVisualizer.setLogScale(true);
                } else {
                    barVisualizer.setLogScale(false);
                }
            }
        });


        TextView xValue = view.findViewById(R.id.x_value);
        xValue.setText(barVisualizer.getDensity() + "");
        TextView yValue = view.findViewById(R.id.y_value);
        yValue.setText(barVisualizer.getBASE_SCALE()  + "");
        final SeekBar x_axis = view.findViewById(R.id.x_scale);
        x_axis.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                //Log.d("STATE",  String.valueOf(progress));
                barVisualizer.setDensity(progress + 64);
                xValue.setText(String.valueOf(progress +64));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        final SeekBar y_axis = view.findViewById(R.id.y_scale);
        y_axis.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                //Log.d("STATE",  String.valueOf(progress));
                int value = (progress + 8) * 4;
                barVisualizer.setBASE_SCALE(value);
                yValue.setText(String.valueOf(value));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        Spinner spinner = view.findViewById(R.id.window_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.window_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                String item = (String) parent.getItemAtPosition(position);
                switch (item) {
                    case "Hann":
                        barVisualizer.setWindow(new HannWindow());
                        break;
                    case "Hamming":
                        barVisualizer.setWindow(new HammingWindow());
                        break;
                    case "Scaled Hamming":
                        barVisualizer.setWindow(new ScaledHammingWindow());
                        break;
                    case "BartlettHann":
                        barVisualizer.setWindow(new BartlettHannWindow());
                        break;
                    case "BlackManHarris":
                        barVisualizer.setWindow(new BlackmanHarrisNuttall());
                        break;
                    case "None":
                        barVisualizer.setWindow(null);
                        break;
                }
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

    }

    public void replay(View view) {
        if (mediaPlayer != null) {
            mediaPlayer.seekTo(0);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }
}
