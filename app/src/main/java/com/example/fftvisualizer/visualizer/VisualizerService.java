package com.example.fftvisualizer.visualizer;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.media.audiofx.Visualizer;

import com.example.fftvisualizer.tools.bluetooth.SerialListener;
import com.example.fftvisualizer.tools.bluetooth.SerialService;
import com.example.fftvisualizer.tools.bluetooth.SerialSocket;

import org.nd4j.linalg.api.ndarray.INDArray;

import java.nio.ByteBuffer;
import java.util.Arrays;

import static android.media.audiofx.Visualizer.MEASUREMENT_MODE_PEAK_RMS;
import static android.media.audiofx.Visualizer.SCALING_MODE_AS_PLAYED;

public class VisualizerService extends SerialService {
    protected Visualizer visualizer;
    protected RollingWindow rollingWindow;
    protected byte[] bytes;
    protected INDArray[] pixels;
    protected float[] fftData;
    private double[] fftTemp;
    protected boolean initialized = false;
    private SerialSocket socket;
    private boolean visualize;

    //Constants
    private static final int fps = 45;
    private static final int MAXPIXELSPACKET = 64; // Do not change this.

    public VisualizerService() {
        super();
    }

    public void connectSocket(SerialListener listener, String deviceName, Context context, BluetoothDevice device) throws Exception {
        socket = new SerialSocket();
        connect(listener, "Connected to " + deviceName);
        socket.connect(context, this, device);
    }

    public void disconnectSocket() {
        if (socket != null) {
            socket.disconnect();
        }
    }


    public void setPlayerThreaded(int audioSessiond) {
        new Thread(() -> setPlayer(audioSessiond)).start();
    }

    public boolean setPlayer(int audioSessionId) {
        visualizer = new Visualizer(0);
        visualizer.setEnabled(false);
        visualizer.setCaptureSize(44100 / fps);
        this.visualizer.setScalingMode(SCALING_MODE_AS_PLAYED);
        this.visualizer.setMeasurementMode(MEASUREMENT_MODE_PEAK_RMS);

        visualizer.setEnabled(true);
        byte[] tmp = new byte[visualizer.getCaptureSize()];
        visualizer.getWaveForm(tmp);
        bytes = tmp;

        rollingWindow = new RollingWindow(bytes.length);
        //pixels = rollingWindow.getPixels();
        initialized = true;
        return true;
    }

    private void calculate(byte[] bytes) {
        if (!initialized) {
            return;
        }
        int n = bytes.length;
        if (fftData == null)
            fftData = new float[n];
        if (fftTemp == null)
            fftTemp = new double[n];

        for (int i = 0; i < n; i++) {
            int tmp = (int) (bytes[i]) & 0xFF;
            fftData[i] = (float) ((tmp) - 128);
            //fftTemp[i] = fftData[i];
        }
        rollingWindow.run(fftData);
        //pixels = rollingWindow.getPixels();
        this.bytes = bytes;
    }

    public boolean initialized() {
        return initialized;
    }

    public void startVisualizing() {
        if (!initialized)
            return;
        visualize = true;
        //final int TICKS_PER_SECOND = 60;
        final int SKIP_TICKS = 1000 / fps;
        final int MAX_FRAMESKIP = 5;
        Thread thread = new Thread() {
            @Override
            public void run() {
                double next_game_tick = System.currentTimeMillis();
                int loops;
                while (visualize) {
                    loops = 0;
                    while (System.currentTimeMillis() > next_game_tick && loops < MAX_FRAMESKIP) {
                        byte[] tmp = new byte[visualizer.getCaptureSize()];
                        visualizer.getWaveForm(tmp);
                        calculate(tmp);
                        parseForStrip();
                        next_game_tick += SKIP_TICKS;
                        loops++;
                    }
                }
            }
        };
        thread.setPriority(10);
        thread.start();
    }
    public void stopVisualize() {
        visualize = false;
    }

    private void parseForStrip() {
        if(!visualize)
            return;
        int[] r = rollingWindow.r.data().asInt();
        int[] g = rollingWindow.g.data().asInt();
        int[] b = rollingWindow.b.data().asInt();
        if ((r.length != g.length) || (g.length != b.length))
            throw new IllegalArgumentException();

        byte[] allByteArray = new byte[((MAXPIXELSPACKET-1)*4) + 2]; //-1 for index correction, *4 for IRGB and +2 for '<'/'>'
        ByteBuffer buff = ByteBuffer.wrap(allByteArray);

        int len = r.length;
        int timeToRefresh = 0;
        for (int i = 0; i < len; i++) {
            byte ledIndex = (byte) ((i) & 0xff);
            byte ledR = (byte) (r[i] & 0xff);
            byte ledG = (byte) (g[i] & 0xff);
            byte ledB = (byte) (b[i] & 0xff);
            byte[] listTest = {ledIndex,ledR,ledG,ledB};
            if (timeToRefresh==0)
                buff.put((byte) '{');
            buff.put(listTest);
            timeToRefresh++;
            if (timeToRefresh == MAXPIXELSPACKET -1 || i == len-1) {
                if (socket != null) {
                    try {
                        buff.put((byte) '}');
                        byte[] data = buff.array();
                        socket.write(data);
                        buff.clear();
                        timeToRefresh=0;
                        System.out.println(Arrays.toString(data));
                        continue;
                    } catch (Exception e) {
                        onSerialIoError(e);
                    }
                }
            }
        }
    }
}
