package com.example.fftvisualizer.visualizer;

import com.example.fftvisualizer.tools.filters.ExpFilter;
import com.example.fftvisualizer.tools.melbank.MelFrequencyFilterBank;
import com.example.fftvisualizer.tools.melbank.props.DoubleData;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.ops.impl.transforms.Pad;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;

import be.tarsos.dsp.util.fft.FFT;
import be.tarsos.dsp.util.fft.HannWindow;

import static org.nd4j.linalg.ops.transforms.Transforms.abs;
import static org.nd4j.linalg.ops.transforms.Transforms.pow;
import static org.nd4j.linalg.ops.transforms.Transforms.reverse;

public class RollingWindow {
    //private static final int samples_per_frame = (int) 44100/60;
    private int samples_per_frame = 1024;
    private static final int ROLLING_HISTORY = 1;
    private static final int FFT_BINS = 12;
    private static final int LEDS = 57;
    //private static final int LEDS = 64;
    private INDArray y_roll;
    private int N_original;
    private FFT fft;
    //private DoubleFFT_1D fft;
    private MelFrequencyFilterBank melbank;
    //private MFCC melbankObject;
    //private INDArray melbankFilterA;
    private INDArray[] pixels;

    private INDArray prev_spectrum;
    private ExpFilter gain_mel;
    private ExpFilter smoothing_mel;
    private ExpFilter r_filt;
    private ExpFilter g_filt;
    private ExpFilter b_filt;
    private ExpFilter common_mode;
    private ExpFilter gain_value;
    private ExpFilter p;

    //visualize
    public INDArray r;
    public INDArray g;
    public INDArray b;
    INDArray norm_linspace;
    INDArray mel;

    LinearInterpolator li;

    /***
     * Made by Jaan Taponen (jaan.taponen@hotmail.com)
     * Decays defines how quickly the effect fades out and rise how "easily" it's seen on the spectrum.
     * + means much the effect is applied
     * - means how much lesser the effect is shown
     * For example: decay of 0.1 and dise of .99 meand the the led turn on/off very quickly.
     * @param size
     */
    public RollingWindow(int size) {
        Nd4j.setDataType(DataType.DOUBLE);
        samples_per_frame = size;
        //y_roll = Nd4j.rand(ROLLING_HISTORY, samples_per_frame);
        y_roll = Nd4j.rand(ROLLING_HISTORY, samples_per_frame).div(1e16); //for testing
        prev_spectrum = Nd4j.zeros(LEDS/2).add(0.01);//np.tile(0.01, config.N_PIXELS // 2)

        gain_value = new ExpFilter(Nd4j.zeros(FFT_BINS).add(300), 0.1,0.2); //add for more broader gain

        gain_mel = new ExpFilter(Nd4j.zeros(FFT_BINS).add(1e-1), 0.01, 0.99);
        smoothing_mel = new ExpFilter(Nd4j.zeros(FFT_BINS).add(1e-1), 0.5, 0.99);

        //common_mode = new ExpFilter(Nd4j.zeros(LEDS/2).add(0.1), 0.8,0.01);
        common_mode = new ExpFilter(Nd4j.zeros(LEDS/2).add(0.1), 0.9,0.01);
        r_filt = new ExpFilter(Nd4j.zeros(LEDS/2).add(0.01), 0.2,0.99);

        g_filt = new ExpFilter(Nd4j.zeros(LEDS/2).add(0.01), 0.05,0.01);
        b_filt = new ExpFilter(Nd4j.zeros(LEDS/2).add(0.01), 0.1,0.5);
        //hannWindow = new doubleHannWindow();
        li =  new LinearInterpolator();

        run(Nd4j.zeros(samples_per_frame).add(1e-16));
    }

    public void run(INDArray y)  {
        //y = pow(y.div(y.length()), 15);
        //normalize(y, 1); //otherwise the filters will go haywire
        INDArray y_data = updateRollingWindow(y);
        INDArray mel = calculateFFTAndMel(y_data);
        visualizeSpectrum(mel);
    }

    public void run(float[] audioSamples)  {
        INDArray y = Nd4j.create(audioSamples);
        run(y);
    }


    private void normalize (INDArray a, double size) {
        double max = (double) a.minNumber();
        double min = (double) a.maxNumber();
        //double[] aNormalized = new double[(int)a.length()];
        for(int i = 0; i < a.length(); i++) {
            double normalized = size - ((a.getDouble(i) - min) / (max - min));
            a.putScalar(i, normalized);
        }
    }

    private INDArray updateRollingWindow(INDArray y) {
        for (int i = 0; i < y_roll.rows()-1; i++) {
            INDArray b = y_roll.getRow(i).get(NDArrayIndex.interval(0, y_roll.getRow(i).length()-1));
            INDArray c = y_roll.getRow(i).get(NDArrayIndex.interval(1, y_roll.getRow(i).length()));
            b.assign(c);
        }
        y_roll.getRow(y_roll.rows()-1).assign(y.dup());
        INDArray y_data = Nd4j.toFlattened(y_roll);

        int N = (int)y_data.length();
        N_original = N;
        if(!isPowerOfTwo(N)) {
            N = closestPowerOfTwoAbove(N);
            int sides = (int)(N-y_data.length());
            y_data= Nd4j.pad(y_data, new int[]{0, sides}, Pad.Mode.CONSTANT, 0);
        }

        return y_data;
    }

    private INDArray calculateFFTAndMel(INDArray y_data) {
        float[] fftResult = y_data.data().asFloat();
        //double[] fftResult = y_data.data().asDouble();
        if(fft == null) {
            fft = new FFT(fftResult.length, new HannWindow());
            //fft = new DoubleFFT_1D(fftResult.length);
        }
        //hannWindow.apply(fftResult);
        fft.forwardTransform(fftResult);


        /*float[] Magtmp = melbankObject.magnitudeSpectrum(fftResult);
        float[] melTmp = melbankObject.melFilter(Magtmp);

        double[] a = new double[Magtmp.length];
        for(int i = 0; i < Magtmp.length; i++) {
            a[i] = Magtmp[i];
        }
        double[] b = new double[melTmp.length];
        for(int i = 0; i < melTmp.length; i++) {
            b[i] = melTmp[i];
        }

        //fft.forwardTransform(fftResult);

        double[] tmp = new double[N_original];
        for(int i = 0; i < N_original; i++) {
            tmp[i] = Math.abs(fftResult[i]);
        }*/

        double[] tmp = new double[N_original / 2];
        for(int i = 1; i < N_original / 2 + 1; i++) tmp[i - 1] = Math.abs(fftResult[i]);

        INDArray fftTemp = Nd4j.create(tmp);
        //fftTemp = abs(fftTemp.get(NDArrayIndex.interval(1, (N_original / 2) + 1))); //strip out DC and cut at Nyquist(not from padded values)

        if(melbank == null) {
            //melbankObject = new MFCC(samples_per_frame, 44100, 0, FFT_BINS, 20, 16000);
            melbank = new MelFrequencyFilterBank(20, 18000, FFT_BINS);
        }
        DoubleData melbankDst = melbank.process(new DoubleData(tmp, 41000, 0));
        INDArray melbankFilter = Nd4j.create(melbankDst.getValues());
        //INDArray melbankFilter = Nd4j.reverse(Nd4j.create(b));

        /*mel = np.atleast_2d(YS).T * dsp.mel_y.T
         *    >>> a = numpy.array([1,2,3,4])
         *    >>> b = numpy.array([5,13])
         *    >>> numpy.atleast_2d(a).T * b.T
         *    array([[ 5, 13],
         *           [10, 26],
         *           [15, 39],
         *           [20, 52]])
         *    >>> a = numpy.array([1,2,3,4,5])
         *    >>> numpy.atleast_2d(a).T * b.T
         *    array([[ 5, 13],
         *           [10, 26],
         *           [15, 39],
         *           [20, 52],
         *           [25, 65]])
         *   Creating a matrix where row len is defined by the FFT data and col len by melbank(24 in this case).
         *   Then placing melbank values to each row and multiplying with next fftvalue.
         *   In case of where fftData is 765 long(stripped from dc/nyquist and the added padding lenght) and
         *   the FFT bins are set to 24 the end result is 765 x 24 matrix. (Numpy len() only reports the row count).
         * */

        INDArray mel = fftTemp.reshape(fftTemp.length(), 1).mmul(melbankFilter.reshape(1, melbankFilter.length()));
        mel = pow(mel.sum(0),2);
        gain_mel.update(Nd4j.zeros(mel.length()).add(mel.maxNumber())); //THE FUCKING MISTAKE WAS HERE!!!!
        mel = mel.div(gain_mel.getValue()); //this is not correct without blur

        mel = smoothing_mel.update(mel); //this wont do anything
        //normalize(mel);
        return mel;
    }

    private void visualizeSpectrum(INDArray y) {
        y = interpolate(y.dup(), LEDS / 2); //multiplying by 100 for decimal point fix for Math commons interpolator.
        common_mode.update(y);
        INDArray diff = y.sub(prev_spectrum);
        prev_spectrum = y.dup();


        r = r_filt.update(y.sub(common_mode.getValue()));
        g = abs(diff);//.mul(10);
        b = b_filt.update(y.dup());//.mul(10));

        r = Nd4j.concat(0, reverse(r, true), r);
        g = Nd4j.concat(0, reverse(g, true), g);
        b = Nd4j.concat(0, reverse(b, true), b);

        this.r = r.mul(255).dup(); this.g = g.mul(255).dup(); this.b = b.mul(255).dup();
        //return new INDArray[]{r, g, b};
    }

    /*
     * "Intelligently" scaling arrays.
     * https://stackoverflow.com/questions/36523396/interpolate-function-using-apache-commons-math/36523685
     *
     * */
    private INDArray interpolate(INDArray y, int newLength) {
        if (y.length() == newLength)
            return y;
        INDArray x_old = normalized_linspace((int)y.length()); //this could be an issue if the len returns wrong
        INDArray x_new = normalized_linspace(newLength);
        PolynomialSplineFunction psf = li.interpolate(x_old.data().asDouble(), y.data().asDouble());

        double[] xi = x_new.data().asDouble();
        double[] yi = new double[xi.length];
        for (int i = 0; i < xi.length; i++) {
            yi[i] = psf.value(xi[i]);
        }
        return Nd4j.create(yi);
    }


    private INDArray normalized_linspace(int size) {
        if (norm_linspace == null || (int)norm_linspace.length() != size) {
            norm_linspace = Nd4j.linspace(0, 1, size);
        }
        return norm_linspace;
    }
    public int getSamples_per_frame() {
        return samples_per_frame;
    }
    private static int closestPowerOfTwoAbove(int N) {
        return 1 << (int) Math.ceil(Math.log(N) / Math.log(2)); }
    private static boolean isPowerOfTwo(int N) {
        final int maxBits = 32;
        int n = 2;
        for (int i = 2; i <= maxBits; i++) {
            if (n == N)
                return true;
            n <<= 1;
        }
        return false;
    }
    /*public INDArray[] getPixels() {
        return pixels;
    }*/
}
