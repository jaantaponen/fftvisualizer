package com.example.fftvisualizer.tools.utils;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

/**
 * this class needs Int type array to bytearray converter for udp
 * the udp sender
 */
public class DataSender extends IntentService {
    private static final String TAG = "com.example.fttvisualizer";
    private boolean sendData = false; //debugging purpose
    private static final Integer LEDS = 60;
    private static final String HOST = "192.168.43.231";
    private static final Integer PORT = 7777;
    public int[] ledArray = new int[LEDS*4];

    public DataSender() {
        super("Helloworld");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public boolean stopService(Intent name) {
        setAllColor(new int[]{0,0,0});

        return super.stopService(name);
    }

    @Override
    public void onDestroy() {
        sendData = false;
        //Toast.makeText(this, "Service stopping", Toast.LENGTH_SHORT).show();
        Log.d("STATE", "Datasender service destoyed!");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
        Log.d("STATE", "Datasender service Started!");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("STATUS", "trying to send packet");
        sendData = true;
        //Getting the rgb values from home fragment.
        int[] color = intent.getIntArrayExtra("color");

        String mode = intent.getStringExtra("mode");
        if (mode.equals("colorSwitch")) {
            setAllColor(color);
            updateESP();
        } else if (mode.equals("effect")) {
            int i = 0;
            while (i < intent.getIntExtra("repeat", 1)) {
                pulseEffect(color, intent.getIntExtra("eyeSize", 4));
                i++;
                wait(1000); //this should be adjusted.
            }
        }
    }

    public boolean setAllColor(int[] color) {
        if (color != null) {
            if (color.length != 2) {
                int count = 0;
                for (int x = 0; x < ledArray.length; x += 4) {
                    ledArray[x] = count; //Index of pixel to chance
                    ledArray[x + 1] = color[0]; //Pixel red
                    ledArray[x + 2] = color[1]; //green
                    ledArray[x + 3] = color[2]; //blue,
                    count++;
                }
                return true;
            }
        }
        return false;
    }

    private void setPixel(int index, int r, int g, int b) {
        index = index*4;
        ledArray[index+1] = r;
        ledArray[index+2] = g;
        ledArray[index+3] = b;
    }

    //TODO implement array sender
    private void updateESP() {
        try {
            InetAddress address = InetAddress.getByName(HOST);
            // Create a datagram socket, send the packet through it, close it.
            DatagramSocket dsocket = new DatagramSocket();

            byte[] allByteArray = new byte[ledArray.length];
            ByteBuffer buff = ByteBuffer.wrap(allByteArray);
            for (int i = 0; i < ledArray.length; i+=4) {
                byte test1 = (byte) (ledArray[i] & 0xff);
                byte test2 = (byte) (ledArray[i+1] & 0xff);
                byte test3 = (byte) (ledArray[i+2] & 0xff);
                byte test4 = (byte) (ledArray[i+3] & 0xff);
                byte[] listTest = {test1,test2,test3,test4};
                //concencating...
                buff.put(listTest);
            }
            byte[] combined = buff.array();

            DatagramPacket packet = new DatagramPacket(combined, combined.length,
                    address, PORT);
            dsocket.send(packet);
            dsocket.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }



    public static void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public boolean pulseEffect(int[] color, int eyesize) {
        if (color != null) {
            if (color.length != 2) {
                int red = color[0];
                int green = color[1];
                int blue = color[2];

                for (int i = 0; i <= LEDS - eyesize -2; i++) {
                    setAllColor(new int[]{0, 0, 0});

                    for (int j = 1; j < eyesize; j++) {
                        setPixel(i+j,red, green, blue);
                    }
                    setPixel(i+eyesize+1, red, green, blue);
                    updateESP();
                    wait(10);
                }

                wait(500); //return delay

                for (int i = LEDS - eyesize -2; i > 0; i--) {
                    setAllColor(new int[]{0, 0, 0});

                    for (int j = 1; j <= eyesize; j++) {
                        setPixel(i+j,red, green, blue);
                    }
                    setPixel(i+eyesize+1, red, green, blue);
                    updateESP();
                    wait(10);
                }

                wait(500); //return delay

            }
        }
        return false;
    }

}
