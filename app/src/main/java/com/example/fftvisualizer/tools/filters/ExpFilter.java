package com.example.fftvisualizer.tools.filters;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.conditions.Conditions;

/***
 *  Made by Jaan Taponen. Simple exponential filter made with NDJ4.
 *  MIT License.
 *
 */

public class ExpFilter {
    private INDArray value;
    private double alpha_decay = 0.5;
    private double alpha_rise = 0.5;
    private double startDoubleValue = 0.0;


    public ExpFilter(INDArray val, double alpha_decay, double alpha_rise) {
        this.value = val;
        //checkValues(alpha_decay, alpha_rise);
        this.alpha_decay = alpha_decay;
        this.alpha_rise = alpha_rise;
    }

    public ExpFilter(INDArray val, double alpha_decay, double alpha_rise, double scaler) {
        this.value = val;
        this.alpha_decay = alpha_decay * scaler;
        this.alpha_rise = alpha_rise * scaler;
    }

    public ExpFilter(double val, double alpha_decay, double alpha_rise) {
        this.startDoubleValue = val;
        //checkValues(alpha_decay, alpha_rise);
        this.alpha_decay = alpha_decay;
        this.alpha_rise = alpha_rise;
    }

    /*private void checkValues(double a, double b) {
        if (!((a > 0.0 && a < 1.0) || (b > 0.0 && b < 1.0)))
            throw new IllegalArgumentException();
    }*/

    public INDArray getValue() {
        return this.value.dup();
    }

    public double getStartDoubleValue() {
        return startDoubleValue;
    }

    /**
     *         if isinstance(self.value, (list, np.ndarray, tuple)):
     *             alpha = value - self.value
     *             alpha[alpha > 0.0] = self.alpha_rise
     *             alpha[alpha <= 0.0] = self.alpha_decay
     *
     *         self.value = alpha * value + (1.0 - alpha) * self.value
     *         return self.value
     * */
    public INDArray update(INDArray a) {
        INDArray updateValues = a.dup();
        INDArray alpha = updateValues.sub(this.value);

        /*INDArray riseArray = Nd4j.zeros(alpha.length()).add(alpha_rise);
        INDArray decayArray = Nd4j.zeros(alpha.length()).add(alpha_decay);
        alpha.replaceWhere(riseArray, Conditions.greaterThan(0.0));
        alpha.replaceWhere(decayArray, Conditions.lessThan(alpha_rise));*/

        alpha.replaceWhere(Nd4j.zeros(alpha.length()).add(alpha_rise), Conditions.greaterThan(0.0));
        alpha.replaceWhere(Nd4j.zeros(alpha.length()).add(alpha_decay), Conditions.lessThanOrEqual(1.0));
        //alpha.replaceWhere(Nd4j.zeros(alpha.length()).add(alpha_decay), Conditions.lessThan(alpha_rise));

        INDArray tmp = alpha.dup().mul(updateValues); //self.value = alpha * value + (1.0 - alpha) * self.value
        INDArray tmp2 = Nd4j.ones(alpha.length()).sub(alpha);
        tmp2 = tmp2.mul(this.value);
        this.value = tmp.add(tmp2);
        return this.value.dup();
    }


    /**
     *      else:
     *          alpha = self.alpha_rise if value > self.value else self.alpha_decay
     *      self.value = alpha * value + (1.0 - alpha) * self.value
     * */
    public double update(double startingPoint) {
        double alpha = startingPoint > this.startDoubleValue ? alpha_rise : alpha_decay;
        this.startDoubleValue = alpha * startingPoint + (1.0- alpha) * this.startDoubleValue;
        return this.startDoubleValue;
    }
}
