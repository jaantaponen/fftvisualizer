package com.example.fftvisualizer.tools.filters;

/**
 * A Hann window function.
 *
 * @author Damien Di Fede
 * @author Corban Brook
 * @see <a href="http://en.wikipedia.org/wiki/Window_function#Hann_window">The
 *      Hann Window</a>
 */
public class doubleHannWindow extends WindowFunction {
    /** Constructs a Hann window. */
    public doubleHannWindow() {
    }

    protected double value(int length, int index) {
        //equal to 0.5 - 0.5 * Math.cos (TWO_PI * index / (length-1f));
        return 0.5f * (1f - (double) Math.cos(TWO_PI * index / (length - 1f)));
    }
}