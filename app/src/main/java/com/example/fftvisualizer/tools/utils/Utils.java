package com.example.fftvisualizer.tools.utils;

import android.util.Log;

public class Utils {
    private long elapsed = 0;
    private long previous = 0;
    private int called = 0;

    public void countCalls() {

        long current = System.currentTimeMillis();
        elapsed += (current - previous);
        if (elapsed >= 1000) {
            Log.d("STATE", "Called :" + called + " per second");
            called = 0;
            elapsed = 0;
        }
        previous = current;
        called ++;
    }

    public static int closestPowerOfTwoAbove(int N) {
        return 1 << (int) Math.ceil(Math.log(N) / Math.log(2));
    }

    public static boolean isPowerOfTwo(int N) {
        final int maxBits = 32;
        int n = 2;
        for (int i = 2; i <= maxBits; i++) {
            if (n == N)
                return true;
            n <<= 1;
        }
        return false;
    }

    public static boolean allElementsAreZero(byte[] array) {
        for (byte b : array) {
            if (b != 0)
                return false;
        }
        return true;
    }

    public static void logArray(int[] a) {
        String prnt = "[";
        for (int i = 0; i < a.length; i++) {
            prnt += a[i] + ", ";
        }
        prnt += "]";
        Log.d("STATE", prnt);
    }
    public static void logArray(float[] a) {
        String prnt = "[";
        for (int i = 0; i < a.length; i++) {
            prnt += a[i] + ", ";
        }
        prnt += "]";
        Log.d("STATE", prnt);
    }
    public static void logArray(double[] a) {
        String prnt = "[";
        for (int i = 0; i < a.length; i++) {
            prnt += a[i] + ", ";
        }
        prnt += "]";
        Log.d("STATE", prnt);
    }
    public static void logArray(byte[] a) {
        String prnt = "[";
        for (int i = 0; i < a.length; i++) {
            prnt += a[i] + ", ";
        }
        prnt += "]";
        Log.d("STATE", prnt);
    }


    private int[] argbToRGB(int[] a) {
        int[] ret = new int[3];
        ret[0] = a[1];
        ret[1] = a[2];
        ret[2] = a[3];
        return ret;
    }

}
